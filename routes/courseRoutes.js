const express = require("express");
const router = express.Router();


const courseControllers = require("../controller/courseControllers")
//console.log(courseControllers);
const auth = require("../auth");

const { verify, verifyAdmin } = auth;


router.get('/', verify, verifyAdmin, courseControllers.getAllCourses)
router.post('/', verify, verifyAdmin, courseControllers.addCourse)


router.get('/activeCourses', courseControllers.getActiveCourses);
router.get('/getSingleCourse/:courseId', courseControllers.getSingleCourse);

router.put("/updateCourse/:courseId", verify, verifyAdmin, courseControllers.updateCourse);

router.delete("/archiveCourse/:courseId", verify, verifyAdmin,courseControllers.archiveCourse);


module.exports = router;