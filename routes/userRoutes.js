const express = require("express");
const router = express.Router();

const auth = require("../auth");
const {verify} = auth;


const userControllers = require ("../controller/userControllers")
//console.log(userControllers);


router.get('/', userControllers.getUsers)

router.post('/', userControllers.registerUser)
router.get('/details', verify,userControllers.getUserDetails)
router.post('/login', userControllers.loginUser)


router.post('/checkEmail', userControllers.checkEmail)

router.post('/enroll', verify, userControllers.enroll);



module.exports = router;