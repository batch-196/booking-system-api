
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course")

module.exports.registerUser = (req, res) => {
    //res.send("This route will create a new user document");
    //console.log(req.body);

    const hashedPw = bcrypt.hashSync(req.body.password, 10);
    // console.log(hashedPw);

    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo

    })

    newUser.save()
        .then(result => res.send(result))
        .catch(error => res.send(error))


};

module.exports.getUserDetails = (req, res) => {
    //res.send("This route will get all user documents");
    //User.findOne({ _id: req.body.id })

    console.log(req.user)
    User.findById(req.user.id)
        .then(result => res.send(result))
        .catch(error => res.send(error))

};

module.exports.getUsers = (req, res) => {
    //res.send("This route will get all user documents");

    User.find({})
        .then(result => res.send(result))
        .catch(error => res.send(error))

};

module.exports.loginUser = (req, res) => {
    //console.log(req.body);

    User.findOne({ email: req.body.email })
        .then(foundUser => {

            if (foundUser === null) {
                return res.send({ message: "No User Found." })
            } else {
                //console.log(foundUser)
                //res.send(foundUser)

                const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

                //console.log(isPasswordCorrect);

                if (isPasswordCorrect) {
                    //console.log("We will create a token for the use if the password is correct")
                    return res.send({ accessToken: auth.createAccessToken(foundUser) });
                } else {
                    return res.send({ message: "Incorrect Password" });
                }

            }

        })

};

module.exports.checkEmail = (req, res) => {
    User.findOne({ email: req.body.email })
        .then(result => {
            if (result === null) {
                console.log("Wala");
                return res.send(false)
            } else {

                return res.send(true)
            }
        })
        .catch(error => res.send(error))
};

module.exports.enroll = async (req, res) => {
    //console.log(req.user.id);
    //console.log(req.body.courseId);

    if (req.user.isAdmin) {
        return res.send({ message: "Action Forbidden." });
    }

    let isUserUpdated = await User.findById(req.user.id).then(user => {

        //console.log(user);

        let newEnrollment = {
            courseId: req.body.courseId
        }
        user.enrollments.push(newEnrollment)

        return user.save().then(user => true).catch(err => err.message)
    })

    if (isUserUpdated !== true) {
        return res.send({ message: isUserUpdated });
    }

    let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
        //console.log(course);
        let enrollee = {
            userId: req.user.id
        }
        course.enrollees.push(enrollee);

        return course.save().then(course => true).catch(err => err.message);
    })

    if (isCourseUpdated !== true) {
        return res.send({ message: isCourseUpdated });
    }

    if (isUserUpdated && isCourseUpdated) {
        return res.send({ message: "Thank you for enrolling!" })
    }

    //console.log(isCourseUpdated);
    //console.log(isUserUpdated);
};