const express = require("express");
const mongoose = require('mongoose');
const app = express();
app.use(express.json())
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin1234@cluster0.k6h2z.mongodb.net/bookingAPI?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
});
let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB Connection Error."));
db.once('open', ()=>console.log("Connected to MongoDB"));

const courseRoutes = require('./routes/courseRoutes');
const userRoutes = require('./routes/userRoutes');

app.use('/courses',courseRoutes);
app.use('/users',userRoutes);


app.listen(port, () => console.log(`Express API running at  localhost:4000`))